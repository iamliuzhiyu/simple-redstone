# Simple redstone
![](https://github.com/iamliuzhiyu/SimpleRedstone/blob/main/title.png "title.png")

This texture pack currently changes the texture of the following blocks:

- Piston
- Sticky Piston
- Piston Head
- Redstone Block
- Redstone Dust
- Redstone Lamp
- Redstone Torch
- Redstone Comparator
- Redstone Repeater

Welcome to use this resources pack!
